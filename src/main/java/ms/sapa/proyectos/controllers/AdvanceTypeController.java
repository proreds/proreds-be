package ms.sapa.proyectos.controllers;

import ms.sapa.proyectos.models.AdvanceTypeModel;
import ms.sapa.proyectos.services.AdvanceTypeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/advance-types")
public class AdvanceTypeController {
    private final AdvanceTypeService service;

    public AdvanceTypeController(AdvanceTypeService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<AdvanceTypeModel>> list(){
        return ResponseEntity.ok().body(service.findAll());
    }
    @PostMapping
    public ResponseEntity<AdvanceTypeModel> create(@RequestBody final AdvanceTypeModel model){
        if(model.getAdvanceTypeId()!=null){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().body(service.save(model));
    }
    @PutMapping("/{id}")
    public ResponseEntity<AdvanceTypeModel> update(@RequestBody final AdvanceTypeModel model,
                                               @PathVariable Short id){
        if(model.getAdvanceTypeId()==null){
            return ResponseEntity.badRequest().build();
        }
        if (!model.getAdvanceTypeId().equals(id)){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().body(service.save(model));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable final Short id){
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
    @GetMapping("/{id}")
    public ResponseEntity<AdvanceTypeModel> getById(@PathVariable final Short id){
        return ResponseEntity.ok().body(service.getById(id));
    }
}
