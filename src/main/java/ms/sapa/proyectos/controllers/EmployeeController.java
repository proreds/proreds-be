package ms.sapa.proyectos.controllers;

import jakarta.validation.Valid;
import ms.sapa.proyectos.dto.EmployeeDTO;
import ms.sapa.proyectos.exceptions.BadArgsException;
import ms.sapa.proyectos.services.EmployeeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/v1/employees")
public class EmployeeController {
    private final EmployeeService service;

    public EmployeeController(EmployeeService service) {
        this.service = service;
    }
    @PostMapping
    public ResponseEntity<EmployeeDTO> create(@Valid @RequestBody
                                              final EmployeeDTO dto) throws URISyntaxException {
        if (dto.getEmployeeId() != null) {
            throw new BadArgsException("A new employee cannot have already an id.");
        }
        var createdProject =service.save(dto);
        return ResponseEntity.created(new URI("/v1/employees/"+createdProject.getEmployeeId())).body(createdProject);
    }
    @PutMapping("/{id}")
    public ResponseEntity<EmployeeDTO> edit(@Valid @RequestBody final EmployeeDTO dto,
                                            @PathVariable final Integer id){
        if (dto.getEmployeeId() == null) {
            throw new BadArgsException("Invalid location id, null value not allowed");
        }

        if (!Objects.equals(id, dto.getEmployeeId())) {
            throw new BadArgsException("Invalid id");
        }

        return ResponseEntity.ok().body(service.save(dto));
    }
    @GetMapping("/chiefs")
    public ResponseEntity<List<EmployeeDTO>> listChiefs(){
        return ResponseEntity.ok().body(service.findAllChiefs());
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable final Integer id){
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
