package ms.sapa.proyectos.controllers;

import jakarta.validation.Valid;
import ms.sapa.proyectos.dto.AdvanceDTO;
import ms.sapa.proyectos.dto.ProjectAdvancesDTO;
import ms.sapa.proyectos.dto.ProjectDTO;
import ms.sapa.proyectos.exceptions.BadArgsException;
import ms.sapa.proyectos.services.AdvanceService;
import ms.sapa.proyectos.services.ProjectService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/v1/projects")
public class ProjectController {
    public final ProjectService projectService;
    public final AdvanceService advanceService;

    public ProjectController(ProjectService projectService, AdvanceService advanceService) {
        this.projectService = projectService;
        this.advanceService = advanceService;
    }
    @PostMapping
    public ResponseEntity<ProjectDTO> create(@Valid @RequestBody
                                                 final ProjectDTO dto) throws URISyntaxException {
        if (dto.getProjectId() != null) {
            throw new BadArgsException("A new project cannot have already an id.");
        }
        var createdProject =projectService.save(dto);
        return ResponseEntity.created(new URI("/v1/projects/"+createdProject.getProjectId())).body(createdProject);
    }
    @PutMapping("/{id}")
    public ResponseEntity<ProjectDTO> edit(@Valid @RequestBody final ProjectDTO dto,
                                           @PathVariable final Integer id){
        if (dto.getProjectId() == null) {
            throw new BadArgsException("Invalid project id, null value not allowed");
        }

        if (!Objects.equals(id, dto.getProjectId())) {
            throw new BadArgsException("Invalid id");
        }

        return ResponseEntity.ok().body(projectService.save(dto));
    }
    @GetMapping
    public ResponseEntity<List<ProjectDTO>> list(@RequestParam(name = "filter",required = false)
                                                     final String filter){
        return ResponseEntity.ok().body(filter != null ?
                projectService.list(filter) : projectService.list());
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable final Integer id){
        projectService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/detailed")
    public Page<ProjectAdvancesDTO> listDetailed(@RequestParam(name = "filter",required = false)
                                                                     final String filter,
                                                 @RequestParam(name = "page",defaultValue = "0")
                                                                    final Integer page){
        int size = 5;
        return projectService.findAllDetailed(page,size,filter);
    }
    // Advances
    @GetMapping("/{id}/advances")
    public ResponseEntity<List<AdvanceDTO>> listAdvances(@PathVariable final Integer id){
        return ResponseEntity.ok().body(advanceService.findByProject(id));
    }
}
