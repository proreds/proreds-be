package ms.sapa.proyectos.controllers;

import jakarta.validation.Valid;
import ms.sapa.proyectos.dto.LocationDTO;
import ms.sapa.proyectos.exceptions.BadArgsException;
import ms.sapa.proyectos.services.LocationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/v1/locations")
public class LocationController {
    private final LocationService service;

    public LocationController(LocationService service) {
        this.service = service;
    }
    @PostMapping
    public ResponseEntity<LocationDTO> create(@Valid @RequestBody
                                             final LocationDTO dto) throws URISyntaxException {
        if (dto.getLocationId() != null) {
            throw new BadArgsException("A new location cannot have already an id.");
        }
        var createdProject =service.save(dto);
        return ResponseEntity.created(new URI("/v1/locations/"+createdProject.getLocationId())).body(createdProject);
    }
    @PutMapping("/{id}")
    public ResponseEntity<LocationDTO> edit(@Valid @RequestBody final LocationDTO dto,
                                           @PathVariable final Integer id){
        if (dto.getLocationId() == null) {
            throw new BadArgsException("Invalid location id, null value not allowed");
        }

        if (!Objects.equals(id, dto.getLocationId())) {
            throw new BadArgsException("Invalid id");
        }

        return ResponseEntity.ok().body(service.save(dto));
    }
    @GetMapping
    public ResponseEntity<List<LocationDTO>> list(){
        return ResponseEntity.ok().body(service.findAll());
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable final Integer id){
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
