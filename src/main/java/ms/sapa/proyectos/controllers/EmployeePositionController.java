package ms.sapa.proyectos.controllers;

import ms.sapa.proyectos.models.AdvanceTypeModel;
import ms.sapa.proyectos.models.EmployeePositionModel;
import ms.sapa.proyectos.services.EmployeePositionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/employee-positions")
public class EmployeePositionController {
    private final EmployeePositionService service;

    public EmployeePositionController(EmployeePositionService service) {
        this.service = service;
    }
    @GetMapping
    public ResponseEntity<List<EmployeePositionModel>> list(){
        return ResponseEntity.ok().body(service.findAll());
    }
    @PostMapping
    public ResponseEntity<EmployeePositionModel> create(@RequestBody final EmployeePositionModel model){
        if(model.getPositionId()!=null){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().body(service.save(model));
    }
    @PutMapping("/{id}")
    public ResponseEntity<EmployeePositionModel> update(@RequestBody final EmployeePositionModel model,
                                                   @PathVariable Short id){
        if(model.getPositionId()==null){
            return ResponseEntity.badRequest().build();
        }
        if (!model.getPositionId().equals(id)){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().body(service.save(model));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable final Short id){
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
    @GetMapping("/{id}")
    public ResponseEntity<EmployeePositionModel> getById(@PathVariable final Short id) {
        return ResponseEntity.ok().body(service.getById(id));
    }
}
