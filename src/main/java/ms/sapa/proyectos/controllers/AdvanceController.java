package ms.sapa.proyectos.controllers;

import jakarta.validation.Valid;
import ms.sapa.proyectos.dto.AdvanceDTO;
import ms.sapa.proyectos.dto.LocationDTO;
import ms.sapa.proyectos.exceptions.BadArgsException;
import ms.sapa.proyectos.services.AdvanceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/v1/advances")
public class AdvanceController {
    private final AdvanceService service;

    public AdvanceController(AdvanceService service) {
        this.service = service;
    }
    @PostMapping
    public ResponseEntity<AdvanceDTO> create(@Valid @RequestBody
                                              final AdvanceDTO dto) throws URISyntaxException {
        if (dto.getAdvanceId() != null) {
            throw new BadArgsException("A new advance cannot have already an id.");
        }
        var createdAdvance =service.save(dto);
        return ResponseEntity.created(new URI("/v1/advances/"+createdAdvance.getAdvanceId())).body(createdAdvance);
    }
    @PutMapping("/{id}")
    public ResponseEntity<AdvanceDTO> edit(@Valid @RequestBody final AdvanceDTO dto,
                                            @PathVariable final Long id){
        if (dto.getAdvanceId() == null) {
            throw new BadArgsException("Invalid advance id, null value not allowed");
        }

        if (!Objects.equals(id, dto.getAdvanceId())) {
            throw new BadArgsException("Invalid id");
        }

        return ResponseEntity.ok().body(service.save(dto));
    }
    @GetMapping
    public ResponseEntity<List<AdvanceDTO>> list(){
        return ResponseEntity.ok().body(service.findAll());
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable final Long id){
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
