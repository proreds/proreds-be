package ms.sapa.proyectos.controllers;

import ms.sapa.proyectos.dto.QuadrilleDTO;
import ms.sapa.proyectos.services.QuadrilleService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/v1/quadrilles")
public class QuadrillesController {
    private final QuadrilleService service;

    public QuadrillesController(QuadrilleService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<QuadrilleDTO>> list(){
        return ResponseEntity.ok().body(service.getAllQuadrilles());
    }
    @PostMapping
    public ResponseEntity<QuadrilleDTO> create(@RequestBody final QuadrilleDTO model){
        if(model.getQuadrilleId()!=null){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().body(service.saveQuadrille(model));
    }
    @PutMapping("/{id}")
    public ResponseEntity<QuadrilleDTO> update(@RequestBody final QuadrilleDTO model,
                                                    @PathVariable Integer id){
        if(model.getQuadrilleId()==null){
            return ResponseEntity.badRequest().build();
        }
        if (!model.getQuadrilleId().equals(id)){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().body(service.saveQuadrille(model));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable final Integer id){
        service.deleteQuadrille(id);
        return ResponseEntity.noContent().build();
    }
    @GetMapping("/{id}")
    public ResponseEntity<QuadrilleDTO> getById(@PathVariable final Integer id){
        return ResponseEntity.ok().body(service.getQuadrilleById(id));
    }
}
