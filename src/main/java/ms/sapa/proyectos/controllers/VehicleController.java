package ms.sapa.proyectos.controllers;

import ms.sapa.proyectos.models.EmployeePositionModel;
import ms.sapa.proyectos.models.VehicleModel;
import ms.sapa.proyectos.services.VehicleService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/vehicles")
public class VehicleController {
    private final VehicleService service;

    public VehicleController(VehicleService service) {
        this.service = service;
    }
    @GetMapping
    public ResponseEntity<List<VehicleModel>> list(){
        return ResponseEntity.ok().body(service.findAll());
    }
    @PostMapping
    public ResponseEntity<VehicleModel> create(@RequestBody final VehicleModel model){
        if(model.getVehicleId()!=null){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().body(service.save(model));
    }
    @PutMapping("/{id}")
    public ResponseEntity<VehicleModel> update(@RequestBody final VehicleModel model,
                                                        @PathVariable Short id){
        if(model.getVehicleId()==null){
            return ResponseEntity.badRequest().build();
        }
        if (!model.getVehicleId().equals(id)){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().body(service.save(model));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable final Short id){
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
    @GetMapping("/{id}")
    public ResponseEntity<VehicleModel> getById(@PathVariable final Short id) {
        return ResponseEntity.ok().body(service.getById(id));
    }
}
