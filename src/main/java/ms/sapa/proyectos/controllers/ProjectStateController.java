package ms.sapa.proyectos.controllers;

import ms.sapa.proyectos.models.ProjectStateModel;
import ms.sapa.proyectos.services.ProjectStateService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/project-states")
public class ProjectStateController {
    private final ProjectStateService service;

    public ProjectStateController(ProjectStateService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<ProjectStateModel>> list(){
        return ResponseEntity.ok().body(service.findAll());
    }
    @PostMapping
    public ResponseEntity<ProjectStateModel> create(@RequestBody final ProjectStateModel model){
        if(model.getProjectStateId()!=null){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().body(service.save(model));
    }
    @PutMapping("/{id}")
    public ResponseEntity<ProjectStateModel> update(@RequestBody final ProjectStateModel model,
                                                    @PathVariable Short id){
        if(model.getProjectStateId()==null){
            return ResponseEntity.badRequest().build();
        }
        if (!model.getProjectStateId().equals(id)){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().body(service.save(model));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable final Short id){
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
