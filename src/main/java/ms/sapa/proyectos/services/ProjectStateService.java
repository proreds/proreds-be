package ms.sapa.proyectos.services;

import ms.sapa.proyectos.models.ProjectStateModel;
import ms.sapa.proyectos.repository.ProjectStateRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectStateService {
    private final ProjectStateRepository repository;
    public ProjectStateService(ProjectStateRepository repository) {
        this.repository = repository;
    }
    public List<ProjectStateModel> findAll(){
        return repository.findAll();
    }
    public ProjectStateModel save(ProjectStateModel model){
        return repository.save(model);
    }
    public void delete(Short id){
        repository.deleteById(id);
    }
}
