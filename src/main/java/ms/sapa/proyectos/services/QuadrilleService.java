package ms.sapa.proyectos.services;

import ms.sapa.proyectos.dto.QuadrilleDTO;
import ms.sapa.proyectos.models.QuadrilleModel;
import ms.sapa.proyectos.repository.QuadrilleRepository;
import ms.sapa.proyectos.services.mapper.QuadrilleMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
@Service
public class QuadrilleService {
    private final QuadrilleMapper mapper;
    private final QuadrilleRepository repository;

    public QuadrilleService(QuadrilleMapper mapper, QuadrilleRepository repository) {
        this.mapper = mapper;
        this.repository = repository;
    }
    public QuadrilleDTO saveQuadrille(QuadrilleDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    public List<QuadrilleDTO> getAllQuadrilles() {
        return repository.findAll().stream().map(mapper::toDto).toList();
    }

    public QuadrilleDTO getQuadrilleById(Integer id) {
        return mapper.toDto(Objects.requireNonNull(repository.findById(id).orElse(null)));
    }

    public void deleteQuadrille(Integer id) {
        repository.deleteById(id);
    }
}
