package ms.sapa.proyectos.services;

import ms.sapa.proyectos.dto.LocationDTO;
import ms.sapa.proyectos.repository.LocationRepository;
import ms.sapa.proyectos.services.mapper.LocationMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationService {
    private final LocationRepository repository;
    private final LocationMapper mapper;

    public LocationService(LocationRepository repository, LocationMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }
    public List<LocationDTO> findAll(){
        return repository.findAll().stream()
                .map(mapper::toDto)
                .toList();
    }
    public LocationDTO save(LocationDTO dto){
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }
    public void delete(Integer id){
        repository.deleteById(id);
    }
}
