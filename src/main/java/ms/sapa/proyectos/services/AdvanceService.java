package ms.sapa.proyectos.services;

import ms.sapa.proyectos.dto.AdvanceDTO;
import ms.sapa.proyectos.dto.ProjectDTO;
import ms.sapa.proyectos.models.AdvanceModel;
import ms.sapa.proyectos.models.AdvanceQuadrilleModel;
import ms.sapa.proyectos.models.ProjectModel;
import ms.sapa.proyectos.models.keys.AdvanceQuadrilleId;
import ms.sapa.proyectos.repository.AdvanceRepository;
import ms.sapa.proyectos.services.mapper.AdvanceMapper;
import ms.sapa.proyectos.services.mapper.ProjectMapper;
import ms.sapa.proyectos.services.mapper.QuadrilleMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@Transactional
public class AdvanceService {
    private final AdvanceRepository repository;
    private final AdvanceMapper mapper;
    private final QuadrilleMapper quadrilleMapper;

    public AdvanceService(AdvanceRepository repository, AdvanceMapper mapper,
                          QuadrilleMapper quadrilleMapper) {
        this.repository = repository;
        this.mapper = mapper;
        this.quadrilleMapper = quadrilleMapper;
    }
    @Transactional(readOnly = true)
    public List<AdvanceDTO> findAll(){
        return repository.findAll().stream()
                .map(mapper::toDto)
                .toList();
    }
    public AdvanceDTO save(AdvanceDTO dto){
        var savedAdvance = repository.save(mapper.toEntity(dto));
        savedAdvance.setQuadrilles(
                dto.getQuadrilles()
                        .stream()
                        .map(quadrilleMapper::toEntity)
                        .map((q) -> new AdvanceQuadrilleModel(new AdvanceQuadrilleId(savedAdvance.getAdvanceId(), q.getQuadrilleId()),
                                savedAdvance, q))
                        .collect(Collectors.toSet()));
        return mapper.toDto(repository.save(savedAdvance));
    }
    public void delete(Long id){
        repository.deleteById(id);
    }
    // Advances
    @Transactional(readOnly = true)
    public List<AdvanceDTO> findByProject(Integer id){
        return repository.findByProjectModel_ProjectIdOrderByStartTime(id)
                .stream()
                .map(mapper::toDto)
                .toList();
    }
}
