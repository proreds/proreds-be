package ms.sapa.proyectos.services;

import ms.sapa.proyectos.exceptions.BadArgsException;
import ms.sapa.proyectos.repository.specifications.SpecificationBuilder;
import org.springframework.data.jpa.domain.Specification;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BaseService<T> {
    private final Pattern pattern = Pattern.compile("(\\w+?)(:|>=|<=)('([^']+)'),");
    private final Pattern valPattern = Pattern.compile("'([(*]?)([^(*)]+)([)*]?)'");
    private Map<String,String> joins = new HashMap<>();

    public BaseService() {
    }
    public BaseService(Map<String,String> joins){
        this.joins = joins;
    }

    public Specification<T> formatCriteria(String filter){
        SpecificationBuilder<T> builder = new SpecificationBuilder<>();
        Matcher matcher = pattern.matcher(filter + ",");
        Matcher valMatcher;
        while (matcher.find()) {
            valMatcher = valPattern.matcher(matcher.group(3));
            if(valMatcher.find()){
                builder.with(matcher.group(1),
                        matcher.group(2),
                        valMatcher.group(2),
                        valMatcher.group(1),
                        valMatcher.group(3),
                        joins.get(matcher.group(1)));
            }else{
                throw new BadArgsException("The filter format is incorrect");
            }
        }
        return builder.build();
    }
}
