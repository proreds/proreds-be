package ms.sapa.proyectos.services;

import ms.sapa.proyectos.models.VehicleModel;
import ms.sapa.proyectos.repository.VehicleRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VehicleService {
    private final VehicleRepository repository;
    public VehicleService(VehicleRepository repository) {
        this.repository = repository;
    }
    public VehicleModel save(VehicleModel model) {
        return repository.save(model);
    }
    public List<VehicleModel> findAll() {
        return repository.findAll();
    }
    public VehicleModel getById(Short id) {
        return repository.findById(id).orElse(null);
    }
    public void delete(Short id) {
        repository.deleteById(id);
    }
}
