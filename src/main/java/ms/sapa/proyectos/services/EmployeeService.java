package ms.sapa.proyectos.services;

import ms.sapa.proyectos.dto.EmployeeDTO;
import ms.sapa.proyectos.repository.EmployeeRepository;
import ms.sapa.proyectos.services.mapper.EmployeeMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    private final EmployeeRepository repository;
    private final EmployeeMapper mapper;

    public EmployeeService(EmployeeRepository repository, EmployeeMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }
    public List<EmployeeDTO> findAllChiefs(){
        Short id = 1;
        return repository.findByPosition_PositionIdOrderByNames(id)
                .stream()
                .map(mapper::toDto)
                .toList();
    }
    public EmployeeDTO save(EmployeeDTO dto){
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }
    public void delete(Integer id){
        repository.deleteById(id);
    }
}
