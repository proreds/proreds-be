package ms.sapa.proyectos.services.mapper;

import ms.sapa.proyectos.dto.VehicleAdvanceDTO;
import ms.sapa.proyectos.models.AdvanceModel;
import ms.sapa.proyectos.models.AdvanceVehicleModel;
import ms.sapa.proyectos.models.keys.AdvanceVehicleId;
import org.springframework.stereotype.Component;

@Component
public class AdvanceVehicleMapper{
    public AdvanceVehicleModel toEntity(VehicleAdvanceDTO dto, AdvanceModel advance) {
        var model = new AdvanceVehicleModel();
        model.setId(new AdvanceVehicleId(dto.getVehicle().getVehicleId(), advance.getAdvanceId()));
        model.setAdvanceV(advance);
        model.setVehicleA(dto.getVehicle());
        model.setEmployeesQty(dto.getNumEmployees());
        return model;
    }

    public VehicleAdvanceDTO toDto(AdvanceVehicleModel model) {
        var dto = new VehicleAdvanceDTO();
        dto.setVehicle(model.getVehicleA());
        dto.setNumEmployees(model.getEmployeesQty());
        return dto;
    }
}
