package ms.sapa.proyectos.services.mapper;

import ms.sapa.proyectos.dto.LocationDTO;
import ms.sapa.proyectos.models.LocationModel;
import org.springframework.stereotype.Component;

@Component
public class LocationMapper {
    public LocationDTO toDto(LocationModel entity){
        var dto = new LocationDTO();
        dto.setLocationId(entity.getLocationId());
        dto.setName(entity.getName());
        dto.setDistance(entity.getDistance());
        return dto;
    }
    public LocationModel toEntity(LocationDTO dto){
        var model = new LocationModel();
        model.setLocationId(dto.getLocationId());
        model.setName(dto.getName());
        model.setDistance(dto.getDistance());
        return model;
    }
}
