package ms.sapa.proyectos.services.mapper;

import ms.sapa.proyectos.dto.QuadrilleDTO;
import ms.sapa.proyectos.models.QuadrilleMemberModel;
import ms.sapa.proyectos.models.QuadrilleModel;
import org.springframework.stereotype.Component;

@Component
public class QuadrilleMapper implements CustomMapper<QuadrilleModel,QuadrilleDTO>{
    private final EmployeeMapper employeeMapper;

    public QuadrilleMapper(EmployeeMapper employeeMapper) {
        this.employeeMapper = employeeMapper;
    }

    @Override
    public QuadrilleModel toEntity(QuadrilleDTO quadrilleDTO) {
        var entity = new QuadrilleModel();
        entity.setQuadrilleId(quadrilleDTO.getQuadrilleId());
        entity.setCode(quadrilleDTO.getCode());
        entity.setVehicle(entity.getVehicle());
        return entity;
    }

    public QuadrilleDTO toDto(QuadrilleModel model){
        var dto = new QuadrilleDTO();
        dto.setQuadrilleId(model.getQuadrilleId());
        dto.setCode(model.getCode());
        if(!model.getMembers().isEmpty()){
            dto.setForeman(model.getMembers()
                .stream()
                .map(QuadrilleMemberModel::getEmployeeQ)
                        .filter(employeeQ -> employeeQ.getPosition().getPositionId() == 2)
                .map(employeeMapper::toDto)
                .toList().getFirst());
        }
        dto.setNumEmployees((short) model.getMembers().size());
        dto.setVehicle(dto.getVehicle());
        return dto;
    }
}
