package ms.sapa.proyectos.services.mapper;

import ms.sapa.proyectos.dto.AdvanceDTO;
import ms.sapa.proyectos.dto.ProjectAdvancesDTO;
import ms.sapa.proyectos.dto.ProjectDTO;
import ms.sapa.proyectos.models.EmployeeModel;
import ms.sapa.proyectos.models.ProjectModel;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProjectMapper {
    private final EmployeeMapper employeeMapper;
    private final LocationMapper locationMapper;

    public ProjectMapper(EmployeeMapper employeeMapper, LocationMapper locationMapper) {
        this.employeeMapper = employeeMapper;
        this.locationMapper = locationMapper;
    }

    public ProjectModel toEntity(ProjectDTO dto,
                                 EmployeeModel responsible){
        ProjectModel entity = new ProjectModel();
        entity.setProjectId(dto.getProjectId());
        entity.setProjectCode(dto.getProjectCode());
        entity.setAssignDate(dto.getAssignDate());
        entity.setEndDate(dto.getEndDate());
        entity.setLocation(locationMapper.toEntity(dto.getLocation()));
        entity.setState(dto.getState());
        entity.setResponsible(responsible);
        return entity;
    }
    public ProjectModel toEntity(ProjectDTO dto){
        return toEntity(dto,employeeMapper.toEntity(dto.getResponsible()));
    }
    public ProjectDTO toDto(ProjectModel entity){
        ProjectDTO dto = new ProjectDTO();
        dto.setProjectId(entity.getProjectId());
        dto.setProjectCode(entity.getProjectCode());
        dto.setAssignDate(entity.getAssignDate());
        dto.setEndDate(entity.getEndDate());
        dto.setResponsible(employeeMapper.toDto(entity.getResponsible()));
        dto.setState(entity.getState());
        dto.setLocation(locationMapper.toDto(entity.getLocation()));
        return dto;
    }
    public ProjectAdvancesDTO toDetailedDTO(ProjectModel entity, List<AdvanceDTO> advances){
        ProjectAdvancesDTO dto = new ProjectAdvancesDTO();
        dto.setProjectId(entity.getProjectId());
        dto.setProjectCode(entity.getProjectCode());
        dto.setAssignDate(entity.getAssignDate());
        dto.setEndDate(entity.getEndDate());
        dto.setResponsible(employeeMapper.toDto(entity.getResponsible()));
        dto.setState(entity.getState());
        dto.setLocation(locationMapper.toDto(entity.getLocation()));
        dto.setAdvances(advances);
        return dto;
    }
}
