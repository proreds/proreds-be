package ms.sapa.proyectos.services.mapper;

import ms.sapa.proyectos.dto.AdvanceDTO;
import ms.sapa.proyectos.dto.VehicleAdvanceDTO;
import ms.sapa.proyectos.models.AdvanceModel;
import ms.sapa.proyectos.models.AdvanceQuadrilleModel;
import ms.sapa.proyectos.models.AdvanceVehicleModel;
import ms.sapa.proyectos.models.keys.AdvanceVehicleId;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class AdvanceMapper implements CustomMapper<AdvanceModel, AdvanceDTO>{
    private final ProjectMapper projectMapper;
    private final QuadrilleMapper quadrilleMapper;
    private final AdvanceVehicleMapper advanceVehicleMapper;

    public AdvanceMapper(ProjectMapper projectMapper, QuadrilleMapper quadrilleMapper,
                         AdvanceVehicleMapper advanceVehicleMapper) {
        this.projectMapper = projectMapper;
        this.quadrilleMapper = quadrilleMapper;
        this.advanceVehicleMapper = advanceVehicleMapper;
    }

    @Override
    public AdvanceModel toEntity(AdvanceDTO dto) {
        var model = new AdvanceModel();
        model.setAdvanceId(dto.getAdvanceId());
        model.setStartTime(dto.getStartTime());
        model.setFinishTime(dto.getFinishTime());
        model.setProjectModel(projectMapper.toEntity(dto.getProject()));
        model.setTypeAdvance(dto.getTypeAdvance());
        model.setVehicles(dto.getVehicles()
                .stream()
                .map((vehicle)->advanceVehicleMapper.toEntity(vehicle,model))
                .collect(Collectors.toSet())
                );
        return model;
    }

    @Override
    public AdvanceDTO toDto(AdvanceModel entity) {
        var dto = new AdvanceDTO();
        dto.setAdvanceId(entity.getAdvanceId());
        dto.setStartTime(entity.getStartTime());
        dto.setFinishTime(entity.getFinishTime());
        dto.setProject(projectMapper.toDto(entity.getProjectModel()));
        dto.setTypeAdvance(entity.getTypeAdvance());
        if(entity.getQuadrilles()!=null){
            dto.setQuadrilles(entity.getQuadrilles()
                    .stream()
                    .map(AdvanceQuadrilleModel::getQuadrilleA)
                    .map(quadrilleMapper::toDto)
                    .toList());
        }
        dto.setVehicles(entity.getVehicles()
                .stream()
                .map(advanceVehicleMapper::toDto)
                .toList()
        );
        return dto;
    }
}
