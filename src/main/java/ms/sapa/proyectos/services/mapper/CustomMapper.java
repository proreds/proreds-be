package ms.sapa.proyectos.services.mapper;

public interface CustomMapper<Entity,DTO> {
    Entity toEntity(DTO dto);
    DTO toDto(Entity entity);
}
