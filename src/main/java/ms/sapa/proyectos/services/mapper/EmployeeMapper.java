package ms.sapa.proyectos.services.mapper;

import ms.sapa.proyectos.dto.EmployeeDTO;
import ms.sapa.proyectos.models.EmployeeModel;
import org.springframework.stereotype.Component;

@Component
public class EmployeeMapper {
    public EmployeeModel toEntity(EmployeeDTO dto){
        if(dto == null){
            return null;
        }
        EmployeeModel entity = new EmployeeModel();
        entity.setEmployeeId(dto.getEmployeeId());
        entity.setNames(dto.getNames());
        entity.setPosition(dto.getPosition());
        entity.setIdCard(dto.getIdCard());
        entity.setLastName(dto.getLastName());
        entity.setMotherLastName(dto.getMotherLastName());
        return entity;
    }
    public EmployeeDTO toDto(EmployeeModel entity){
        if(entity == null){
            return null;
        }
        EmployeeDTO dto = new EmployeeDTO();
        dto.setEmployeeId(entity.getEmployeeId());
        dto.setNames(entity.getNames());
        dto.setPosition(entity.getPosition());
        dto.setIdCard(entity.getIdCard());
        dto.setLastName(entity.getLastName());
        dto.setMotherLastName(entity.getMotherLastName());
        return dto;
    }
}
