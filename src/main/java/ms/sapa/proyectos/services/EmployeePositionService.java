package ms.sapa.proyectos.services;


import ms.sapa.proyectos.models.EmployeePositionModel;
import ms.sapa.proyectos.repository.EmployeePositionRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeePositionService {
    private final EmployeePositionRepository repository;

    public EmployeePositionService(EmployeePositionRepository repository) {
        this.repository = repository;
    }

    public EmployeePositionModel save(EmployeePositionModel model) {
        return repository.save(model);
    }
    public List<EmployeePositionModel> findAll() {
        return repository.findAll();
    }
    public EmployeePositionModel getById(Short id) {
        return repository.findById(id).orElse(null);
    }
    public void delete(Short id) {
        repository.deleteById(id);
    }
}
