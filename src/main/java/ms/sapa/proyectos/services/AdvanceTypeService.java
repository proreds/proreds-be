package ms.sapa.proyectos.services;

import ms.sapa.proyectos.models.AdvanceTypeModel;
import ms.sapa.proyectos.repository.AdvanceTypeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AdvanceTypeService {
    private final AdvanceTypeRepository repository;

    public AdvanceTypeService(AdvanceTypeRepository repository) {
        this.repository = repository;
    }
    public AdvanceTypeModel save(AdvanceTypeModel model) {
        return repository.save(model);
    }
    public List<AdvanceTypeModel> findAll() {
        return repository.findAll();
    }
    public AdvanceTypeModel getById(Short id) {
        return repository.findById(id).orElse(null);
    }
    public void delete(Short id) {
        repository.deleteById(id);
    }
}
