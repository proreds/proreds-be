package ms.sapa.proyectos.services;

import ms.sapa.proyectos.dto.ProjectAdvancesDTO;
import ms.sapa.proyectos.dto.ProjectDTO;
import ms.sapa.proyectos.models.ProjectModel;
import ms.sapa.proyectos.repository.ProjectRepository;
import ms.sapa.proyectos.services.mapper.AdvanceMapper;
import ms.sapa.proyectos.services.mapper.EmployeeMapper;
import ms.sapa.proyectos.services.mapper.ProjectMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProjectService extends BaseService<ProjectModel> {
    private final ProjectMapper projectMapper;
    private final ProjectRepository repository;
    private final EmployeeMapper employeeMapper;
    private final AdvanceMapper advanceMapper;
    public ProjectService(ProjectMapper projectMapper
            , ProjectRepository repository, EmployeeMapper employeeMapper, AdvanceMapper advanceMapper) {
        super(Map.of(
                "employeeId","responsible",
                "locationId","location",
                "projectStateId","state"
        ));
        this.projectMapper = projectMapper;
        this.repository = repository;
        this.employeeMapper = employeeMapper;
        this.advanceMapper = advanceMapper;
    }

    public ProjectDTO save(ProjectDTO dto){
        return projectMapper.toDto(repository.save(
                projectMapper.toEntity(dto,employeeMapper.toEntity(dto.getResponsible()))));
    }
    public void delete(Integer id){
        repository.deleteById(id);
    }
    public List<ProjectDTO> list(){
        return repository.findAll().stream()
                .map(projectMapper::toDto)
                .toList();
    }
    public List<ProjectDTO> list(String filter){
        return repository.findAll(formatCriteria(filter))
                .stream()
                .map(projectMapper::toDto)
                .toList();
    }
    public Page<ProjectAdvancesDTO> findAllDetailed(int page, int size, String filter){
        Pageable pageable = PageRequest.of(page, size, Sort.by("assignDate").ascending());
        Page<ProjectModel> projectPage;
        if (filter == null) {
            projectPage = repository.findAll(pageable);
        } else {
            projectPage = repository.findAll(formatCriteria(filter), pageable);
        }

        return projectPage.map(p -> projectMapper.toDetailedDTO(p, p.getAdvances()
                .stream()
                .map(advanceMapper::toDto)
                .collect(Collectors.toList())));
    }
}
