package ms.sapa.proyectos.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import ms.sapa.proyectos.models.ProjectStateModel;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class ProjectAdvancesDTO {
    private Integer projectId;
    private Integer projectCode;

    @JsonFormat(pattern = "MM-dd-yyyy")
    private LocalDate assignDate;

    @JsonFormat(pattern = "MM-dd-yyyy")
    private LocalDate endDate;

    private EmployeeDTO responsible;
    private ProjectStateModel state;
    private LocationDTO location;
    private List<AdvanceDTO> advances;
}
