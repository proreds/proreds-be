package ms.sapa.proyectos.dto;

import lombok.Getter;
import lombok.Setter;
import ms.sapa.proyectos.models.VehicleModel;
import org.springframework.stereotype.Component;

@Getter
@Setter
public class VehicleAdvanceDTO {
    private VehicleModel vehicle;
    private Short numEmployees;
}
