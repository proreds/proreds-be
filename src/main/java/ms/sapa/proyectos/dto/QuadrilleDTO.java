package ms.sapa.proyectos.dto;

import lombok.Getter;
import lombok.Setter;
import ms.sapa.proyectos.models.VehicleModel;

@Getter
@Setter
public class QuadrilleDTO {
    private Integer quadrilleId;
    private String code;
    private VehicleModel vehicle;
    private EmployeeDTO foreman;
    private Short numEmployees;
}
