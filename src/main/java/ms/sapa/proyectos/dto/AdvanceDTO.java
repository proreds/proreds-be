package ms.sapa.proyectos.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import ms.sapa.proyectos.models.AdvanceTypeModel;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class AdvanceDTO {
    private Long advanceId;
    @JsonFormat(pattern = "MM-dd-yyyy HH:mm:ss")
    private LocalDateTime startTime;
    @JsonFormat(pattern = "MM-dd-yyyy HH:mm:ss")
    private LocalDateTime finishTime;
    private ProjectDTO project;
    private AdvanceTypeModel typeAdvance;
    private List<QuadrilleDTO> quadrilles;
    private List<VehicleAdvanceDTO> vehicles;
}
