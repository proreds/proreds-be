package ms.sapa.proyectos.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import ms.sapa.proyectos.models.EmployeeModel;
import ms.sapa.proyectos.models.LocationModel;
import ms.sapa.proyectos.models.ProjectStateModel;

import java.time.LocalDate;

@Getter
@Setter
public class ProjectDTO {
    private Integer projectId;
    private Integer projectCode;

    @JsonFormat(pattern = "MM-dd-yyyy")
    private LocalDate assignDate;

    @JsonFormat(pattern = "MM-dd-yyyy")
    private LocalDate endDate;

    private EmployeeDTO responsible;
    private ProjectStateModel state;
    private LocationDTO location;
}
