package ms.sapa.proyectos.dto;

import lombok.Getter;
import lombok.Setter;
import ms.sapa.proyectos.models.EmployeePositionModel;
@Getter
@Setter
public class EmployeeDTO {
    private Integer employeeId;
    private String names;
    private String lastName;
    private String motherLastName;
    private String idCard;
    private EmployeePositionModel position;
}
