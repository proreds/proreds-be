package ms.sapa.proyectos.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocationDTO {
    private Integer locationId;

    private String name;

    private Float distance;
}
