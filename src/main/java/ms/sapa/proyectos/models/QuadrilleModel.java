package ms.sapa.proyectos.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "quadrilles")
public class QuadrilleModel implements Serializable {
    private static final long serialVersionUID = 131239639351724L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "quadrille_seq")
    @SequenceGenerator(name = "quadrille_seq",sequenceName = "quadrille_seq",allocationSize = 1)
    @Column(name = "quadrille_id")
    private Integer quadrilleId;

    private String code;

    @Column(name = "creation_ts",columnDefinition = "timestamp")
    private LocalDateTime creationTs;

    @Column(name = "update_ts",columnDefinition = "timestamp")
    private LocalDateTime updateTs;

    @ManyToOne
    @JoinColumn(name = "vehicle_id")
    private VehicleModel vehicle;

    @OneToMany(mappedBy = "quadrilleE",fetch = FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.REMOVE},
            targetEntity = QuadrilleMemberModel.class)
    private List<QuadrilleMemberModel> members;
}
