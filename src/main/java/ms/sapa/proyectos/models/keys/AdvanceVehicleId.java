package ms.sapa.proyectos.models.keys;

import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Embeddable
@Getter
@Setter
public class AdvanceVehicleId implements Serializable {
    private static final long serialVersionUID = 459878912398433L;

    private Short vehicleId;

    private Long advanceId;

    public AdvanceVehicleId(Short vehicleId, Long advanceId) {
        this.vehicleId = vehicleId;
        this.advanceId = advanceId;
    }
}
