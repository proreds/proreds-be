package ms.sapa.proyectos.models.keys;

import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
@Embeddable
@Getter
@Setter
public class AdvanceQuadrilleId implements Serializable {
    private static final long serialVersionUID = 45645612398433L;

    private Long advanceId;

    private Integer quadrilleId;

    public AdvanceQuadrilleId() {
    }

    public AdvanceQuadrilleId(Long advanceId, Integer quadrilleId) {
        this.advanceId = advanceId;
        this.quadrilleId = quadrilleId;
    }
}
