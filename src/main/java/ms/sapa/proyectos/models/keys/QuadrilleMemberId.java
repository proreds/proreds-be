package ms.sapa.proyectos.models.keys;

import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
@Embeddable
@Getter
@Setter
public class QuadrilleMemberId implements Serializable {
    private static final long serialVersionUID = 45645645698433L;

    private Integer employeeId;

    private Integer quadrilleId;
}
