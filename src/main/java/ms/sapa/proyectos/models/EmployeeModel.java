package ms.sapa.proyectos.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "employees")
public class EmployeeModel implements Serializable {
    private static final long serialVersionUID = 78393127351724L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "employee_seq")
    @SequenceGenerator(name = "employee_seq",sequenceName = "employee_seq",allocationSize = 1)
    @Column(name = "employee_id")
    private Integer employeeId;

    private String names;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "mother_last_name")
    private String motherLastName;

    @Column(name = "id_card")
    private String idCard;

    @Column(name = "creation_ts",columnDefinition = "timestamp")
    private LocalDateTime creationDate;

    @Column(name = "update_ts",columnDefinition = "timestamp")
    private LocalDateTime updateDate;

    @ManyToOne
    @JoinColumn(name = "position_id")
    private EmployeePositionModel position;
}
