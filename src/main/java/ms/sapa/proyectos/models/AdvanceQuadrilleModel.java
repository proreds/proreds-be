package ms.sapa.proyectos.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import ms.sapa.proyectos.models.keys.AdvanceQuadrilleId;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "advance_quadrille")
public class AdvanceQuadrilleModel implements Serializable {
    private static final long serialVersionUID = 13133366651724L;

    @EmbeddedId
    private AdvanceQuadrilleId id;

    @Column(name = "update_ts",columnDefinition = "timestamp")
    private LocalDateTime updateTs;

    @ManyToOne
    @MapsId("advanceId")
    @JoinColumn(name = "advance_id")
    private AdvanceModel advanceQ;

    @ManyToOne
    @MapsId("quadrilleId")
    @JoinColumn(name = "quadrille_id")
    private QuadrilleModel quadrilleA;

    public AdvanceQuadrilleModel() {

    }
    public AdvanceQuadrilleModel(AdvanceModel advanceQ, QuadrilleModel quadrilleA) {
        this.advanceQ = advanceQ;
        this.quadrilleA = quadrilleA;
    }

    public AdvanceQuadrilleModel(AdvanceQuadrilleId id) {
        this.id = id;
    }

    public AdvanceQuadrilleModel(AdvanceQuadrilleId id, AdvanceModel advanceQ, QuadrilleModel quadrilleA) {
        this.id = id;
        this.advanceQ = advanceQ;
        this.quadrilleA = quadrilleA;
    }
}
