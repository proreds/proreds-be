package ms.sapa.proyectos.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "advance_type")
public class AdvanceTypeModel implements Serializable {
    private static final long serialVersionUID = 13123389651724L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "advance_type_seq")
    @SequenceGenerator(name = "advance_type_seq",sequenceName = "advance_type_seq",allocationSize = 1)
    @Column(name = "advance_type_id",columnDefinition = "smallint")
    private Short advanceTypeId;

    private String description;
}
