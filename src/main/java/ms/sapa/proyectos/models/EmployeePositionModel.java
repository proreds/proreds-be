package ms.sapa.proyectos.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "employee_position")
public class EmployeePositionModel implements Serializable {
    private static final long serialVersionUID = 7387893127351724L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "employee_position_seq")
    @SequenceGenerator(name = "employee_position_seq",sequenceName = "employee_position_seq",allocationSize = 1)
    @Column(name = "position_id")
    private Short positionId;

    private String name;
}
