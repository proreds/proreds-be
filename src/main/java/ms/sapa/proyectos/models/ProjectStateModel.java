package ms.sapa.proyectos.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "project_state")
public class ProjectStateModel implements Serializable {
    private static final long serialVersionUID = 4587589639351724L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "project_state_seq")
    @SequenceGenerator(name = "project_state_seq",sequenceName = "project_state_seq",allocationSize = 1)
    @Column(name = "project_state_id")
    private Short projectStateId;

    private String name;
}
