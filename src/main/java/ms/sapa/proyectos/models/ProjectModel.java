package ms.sapa.proyectos.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Setter
@Getter
@Entity
@Table(name = "projects")
public class ProjectModel implements Serializable {
    private static final long serialVersionUID = 5757858639351724L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "project_seq")
    @SequenceGenerator(name = "project_seq",sequenceName = "project_seq",allocationSize = 1)
    @Column(name = "project_id")
    private Integer projectId;

    @Column(name = "project_code",columnDefinition = "smallint")
    private Integer projectCode;

    @Column(name = "assign_date",columnDefinition = "date")
    private LocalDate assignDate;

    @Column(name = "end_date",columnDefinition = "date")
    private LocalDate endDate;

    @Column(name = "creation_ts",columnDefinition = "timestamp")
    private LocalDateTime creationTS;

    @Column(name = "update_ts",columnDefinition = "timestamp")
    private LocalDateTime updateTS;

    @ManyToOne
    @JoinColumn(name = "responsible_id")
    private EmployeeModel responsible;

    @ManyToOne
    @JoinColumn(name = "state_id")
    private ProjectStateModel state;

    @ManyToOne
    @JoinColumn(name = "location_id")
    private LocationModel location;

    @OneToMany(mappedBy = "projectModel",fetch = FetchType.LAZY,targetEntity = AdvanceModel.class)
    private Set<AdvanceModel> advances;
}
