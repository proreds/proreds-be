package ms.sapa.proyectos.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "locations")
public class LocationModel implements Serializable {
    private static final long serialVersionUID = 782863127351724L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "locations_seq")
    @SequenceGenerator(name = "locations_seq",sequenceName = "locations_seq",allocationSize = 1)
    @Column(name = "location_id")
    private Integer locationId;

    private String name;

    private Float distance;

    @Column(name = "creation_ts",columnDefinition = "timestamp")
    private LocalDateTime creationTs;

    @Column(name = "update_ts",columnDefinition = "timestamp")
    private LocalDateTime updateTs;
}
