package ms.sapa.proyectos.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "vehicles")
public class VehicleModel implements Serializable {
    private static final long serialVersionUID = 57623963935768674L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "vehicles_seq")
    @SequenceGenerator(name = "vehicles_seq",sequenceName = "vehicles_seq",allocationSize = 1)
    @Column(name = "vehicle_id")
    private Short vehicleId;

    private String plate;

    private String description;

    private String color;
}
