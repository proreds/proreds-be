package ms.sapa.proyectos.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "advances")
public class AdvanceModel implements Serializable {
    private static final long serialVersionUID = 13122347351724L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "advance_seq")
    @SequenceGenerator(name = "advance_seq",sequenceName = "advance_seq",allocationSize = 1)
    @Column(name = "advance_id",columnDefinition = "bigserial")
    private Long advanceId;

    @Column(name = "start_time",columnDefinition = "timestamp")
    private LocalDateTime startTime;

    @Column(name = "end_time",columnDefinition = "timestamp")
    private LocalDateTime finishTime;

    @Column(name = "creation_ts",columnDefinition = "timestamp")
    private LocalDateTime creationDate;

    @Column(name = "update_ts",columnDefinition = "timestamp")
    private LocalDateTime updateDate;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private ProjectModel projectModel;

    @ManyToOne
    @JoinColumn(name = "type_id")
    private AdvanceTypeModel typeAdvance;

    @OneToMany(mappedBy = "advanceQ",fetch = FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.REMOVE, CascadeType.MERGE})
    private Set<AdvanceQuadrilleModel> quadrilles;

    @OneToMany(mappedBy = "advanceV",cascade = {CascadeType.PERSIST,CascadeType.REMOVE, CascadeType.MERGE})
    private Set<AdvanceVehicleModel> vehicles;
    public AdvanceModel(){

    }

    public AdvanceModel(Long advanceId) {
        this.advanceId = advanceId;
    }
}
