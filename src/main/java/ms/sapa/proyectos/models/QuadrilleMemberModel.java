package ms.sapa.proyectos.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import ms.sapa.proyectos.models.keys.QuadrilleMemberId;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "quadrille_members")
public class QuadrilleMemberModel implements Serializable {
    private static final long serialVersionUID = 434239654431724L;

    @EmbeddedId
    private QuadrilleMemberId id;

    @ManyToOne
    @MapsId("employeeId")
    @JoinColumn(name = "employee_id")
    private EmployeeModel employeeQ;

    @ManyToOne
    @MapsId("quadrilleId")
    @JoinColumn(name = "quadrille_id")
    private QuadrilleModel quadrilleE;

    @Column(name = "update_ts",columnDefinition = "timestamp")
    private LocalDateTime updateTs;
}
