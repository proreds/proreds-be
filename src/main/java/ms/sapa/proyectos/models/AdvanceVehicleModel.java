package ms.sapa.proyectos.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import ms.sapa.proyectos.models.keys.AdvanceQuadrilleId;
import ms.sapa.proyectos.models.keys.AdvanceVehicleId;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "advance_vehicle")
public class AdvanceVehicleModel implements Serializable {
    private static final long serialVersionUID = 13133368798724L;

    @EmbeddedId
    private AdvanceVehicleId id;

    @Column(name = "update_ts",columnDefinition = "timestamp")
    private LocalDateTime updateTs;

    @Column(name = "employees_qty")
    private Short employeesQty;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("advanceId")
    @JoinColumn(name = "advance_id")
    private AdvanceModel advanceV;

    @ManyToOne
    @MapsId("vehicleId")
    @JoinColumn(name = "vehicle_id")
    private VehicleModel vehicleA;

    public AdvanceVehicleModel(AdvanceVehicleId id, Short employeesQty, AdvanceModel advanceV, VehicleModel vehicleA) {
        this.id = id;
        this.employeesQty = employeesQty;
        this.advanceV = advanceV;
        this.vehicleA = vehicleA;
    }

    public AdvanceVehicleModel() {
    }
}
