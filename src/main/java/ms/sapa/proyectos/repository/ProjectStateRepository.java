package ms.sapa.proyectos.repository;

import ms.sapa.proyectos.models.ProjectStateModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectStateRepository extends JpaRepository<ProjectStateModel,Short> {
}
