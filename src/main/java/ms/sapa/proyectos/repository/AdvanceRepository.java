package ms.sapa.proyectos.repository;

import ms.sapa.proyectos.models.AdvanceModel;
import ms.sapa.proyectos.models.ProjectModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdvanceRepository extends JpaRepository<AdvanceModel,Long> {
    List<AdvanceModel> findByProjectModel_ProjectIdOrderByStartTime(Integer id);
}
