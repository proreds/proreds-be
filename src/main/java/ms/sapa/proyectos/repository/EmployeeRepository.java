package ms.sapa.proyectos.repository;

import ms.sapa.proyectos.models.EmployeeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeModel,Integer> {
    List<EmployeeModel> findByPosition_PositionIdOrderByNames(Short id);
}
