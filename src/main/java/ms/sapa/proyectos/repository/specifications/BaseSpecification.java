package ms.sapa.proyectos.repository.specifications;

import jakarta.persistence.criteria.*;
import org.springframework.data.jpa.domain.Specification;

import java.util.Map;

public class BaseSpecification<T> implements Specification<T> {
    private final SpecSearchCriteria criteria;

    public BaseSpecification(SpecSearchCriteria criteria) {
        this.criteria = criteria;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        From<?,T> accessor;
        if(criteria.getJoin() != null && !criteria.getJoin().isEmpty()){
            accessor = root.join(criteria.getJoin());
        }else{
            accessor = root;
        }
        switch (criteria.getOperation()){
            case GREATER_EQ_THAN:
                return builder.greaterThanOrEqualTo(
                        accessor.get(criteria.getKey()), criteria.getValue());

            case LESS_EQ_THAN:
                return builder.lessThanOrEqualTo(
                        accessor.get(criteria.getKey()), criteria.getValue());

            case EQUALITY:
                return builder.equal(accessor.get(criteria.getKey()), criteria.getValue());

            case CONTAINS:
                if (accessor.get(criteria.getKey()).getJavaType() == String.class) {
                    return builder.like(
                            accessor.get(criteria.getKey()), "%" + criteria.getValue() + "%");
                }
            case STARTS_WITH:
                return builder.like(
                        accessor.get(criteria.getKey()),"%"+criteria.getValue()
                );
            case ENDS_WITH:
                return builder.like(
                        accessor.get(criteria.getKey()),criteria.getValue()+"%"
                );
            case BETWEEN:
                if(criteria.getValue().contains(",")){
                    var values = criteria.getValue().split(",");
                    return builder.between(accessor.get(criteria.getKey()),
                            values[0],values[1]);
                }
                return builder.equal(accessor.get(criteria.getKey()), criteria.getValue());
        }
        return null;
    }
}
