package ms.sapa.proyectos.repository.specifications;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class SpecSearchCriteria {

    private String key;
    private SearchOperation operation;
    private String value;
    private boolean orPredicate;
    private String join;

    public SpecSearchCriteria(String key, SearchOperation operation, String value, boolean orPredicate) {
        this.key = key;
        this.operation = operation;
        this.value = value;
        this.orPredicate = orPredicate;
        this.join = null;
    }
    public SpecSearchCriteria(String key, SearchOperation operation, String value, boolean orPredicate,String join) {
        this.key = key;
        this.operation = operation;
        this.value = value;
        this.orPredicate = orPredicate;
        this.join = join;
    }

    public boolean isOrPredicate() {
        return orPredicate;
    }
}
