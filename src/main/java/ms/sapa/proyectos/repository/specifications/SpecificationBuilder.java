package ms.sapa.proyectos.repository.specifications;

import org.springframework.data.jpa.domain.Specification;

import java.util.*;

public class SpecificationBuilder<T> {
    private final List<SpecSearchCriteria> params;

    public SpecificationBuilder() {
        params = new ArrayList<>();
    }

    public final SpecificationBuilder<T> with(String key, String operation, String value,
                                                String prefix, String suffix) {
        return with(false, key, operation, value, prefix, suffix,null);
    }

    public final SpecificationBuilder<T> with(String key, String operation, String value,
                                              String prefix, String suffix,String join) {
        return with(false, key, operation, value, prefix, suffix,join);
    }

    public final SpecificationBuilder<T> with(boolean orPredicate, String key, String operation,
                                                String value, String prefix, String suffix, String join) {
        SearchOperation op = SearchOperation.getSimpleOperation(operation);
        if (op != null) {
            if (op == SearchOperation.EQUALITY) {
                boolean startWithAsterisk = prefix != null &&
                        prefix.contains(SearchOperation.ZERO_OR_MORE_REGEX.symbol);
                boolean endWithAsterisk = suffix != null &&
                        suffix.contains(SearchOperation.ZERO_OR_MORE_REGEX.symbol);
                boolean inParenthesis = suffix != null && prefix != null &&
                        prefix.contains(SearchOperation.START_GROUP.symbol) &&
                        suffix.contains(SearchOperation.END_GROUP.symbol);
                if(startWithAsterisk && endWithAsterisk){
                    op = SearchOperation.CONTAINS;
                } else if (startWithAsterisk) {
                    op = SearchOperation.STARTS_WITH;
                } else if (endWithAsterisk) {
                    op = SearchOperation.ENDS_WITH;
                } else if (inParenthesis) {
                    op = SearchOperation.BETWEEN;
                }
            }
            params.add(new SpecSearchCriteria(key, op, value,orPredicate,join));
        }
        return this;
    }

    public Specification<T> build() {
        if (params.isEmpty())
            return null;

        Specification<T> result = new BaseSpecification<>(params.getFirst());

        for (int i = 1; i < params.size(); i++) {
            result = params.get(i).isOrPredicate()
                    ? Specification.where(result).or(new BaseSpecification<>(params.get(i)))
                    : Specification.where(result).and(new BaseSpecification<>(params.get(i)));
        }

        return result;
    }
}
