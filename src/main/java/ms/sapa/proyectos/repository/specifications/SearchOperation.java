package ms.sapa.proyectos.repository.specifications;

public enum SearchOperation {
    ZERO_OR_MORE_REGEX("*"),
    START_GROUP("("),
    END_GROUP(")"),
    EQUALITY(":"),
    CONTAINS(":"),
    STARTS_WITH("*"),
    ENDS_WITH("*"),
    GREATER_EQ_THAN(">="),
    LESS_EQ_THAN("<="),
    BETWEEN("()");

    public final String symbol;
    SearchOperation(String symbol) {
        this.symbol=symbol;
    }

    public static SearchOperation getSimpleOperation(String input) {
        return switch (input) {
            case ":" -> EQUALITY;
            case ">=" -> GREATER_EQ_THAN;
            case "<=" -> LESS_EQ_THAN;
            case "*" -> ZERO_OR_MORE_REGEX;
            default -> null;
        };
    }
}
