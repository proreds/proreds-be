package ms.sapa.proyectos.repository;

import ms.sapa.proyectos.models.QuadrilleModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuadrilleRepository extends JpaRepository<QuadrilleModel,Integer> {
}
