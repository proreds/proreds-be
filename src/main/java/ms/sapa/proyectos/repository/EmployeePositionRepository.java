package ms.sapa.proyectos.repository;

import ms.sapa.proyectos.models.EmployeePositionModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeePositionRepository extends JpaRepository<EmployeePositionModel,Short> {
}
