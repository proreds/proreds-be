package ms.sapa.proyectos.repository;

import ms.sapa.proyectos.models.LocationModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends JpaRepository<LocationModel,Integer> {
}
