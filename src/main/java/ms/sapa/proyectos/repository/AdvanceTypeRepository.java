package ms.sapa.proyectos.repository;

import ms.sapa.proyectos.models.AdvanceTypeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdvanceTypeRepository extends JpaRepository<AdvanceTypeModel,Short> {
}
