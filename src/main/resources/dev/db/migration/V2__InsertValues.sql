-- Positions
insert into employee_position values (nextval('employee_position_seq'),'Jefe Faena'),
                                     (nextval('employee_position_seq'),'Capataz'),
                                     (nextval('employee_position_seq'),'Liniero'),
                                     (nextval('employee_position_seq'),'Nivel 1'),
                                     (nextval('employee_position_seq'),'Nivel 2');
-- Employees
insert into employees (employee_id,position_id,names,last_name,mother_last_name,id_card,creation_ts,update_ts)
values (nextval('employee_seq'),1,'José Manuel','Quiroga','Paniagua','8816735',now(),now()),
       (nextval('employee_seq'),1,'Jorge','Sandoval','Quispe','9898784',now(),now()),
       (nextval('employee_seq'),2,'Pedro','Muriel','Zegarra','7987882',now(),now()),
       (nextval('employee_seq'),3,'Victor','Zuniga','Castillo','7868465',now(),now()),
       (nextval('employee_seq'),4,'Josue','Baltazar','Bejarano','7354654',now(),now());
-- Vehicles
insert into vehicles(vehicle_id, plate, description, color)
values (nextval('vehicles_seq'),'4545RTD','Grua verde','green'),
       (nextval('vehicles_seq'),'5465WEW','Grua Roja','red');
-- Quadrilles
insert into quadrilles(quadrille_id, vehicle_id, code, creation_ts, update_ts)
values (nextval('quadrille_seq'),1,'FARE',now(),now()),
       (nextval('quadrille_seq'),2,'GERS',now(),now());

--Quadrille-employee
insert into quadrille_members(employee_id, quadrille_id, update_ts)
values (3,1,now()),
       (4,1,now()),
       (5,1,now());
-- states
insert into project_state (project_state_id, name)
values (nextval('project_state_seq'),'Activo'),
       (nextval('project_state_seq'),'En progreso'),
       (nextval('project_state_seq'),'Cierre físico'),
       (nextval('project_state_seq'),'Facturado'),
       (nextval('project_state_seq'),'Concluido');

-- location
insert into locations (location_id, name, distance, creation_ts, update_ts)
values (nextval('locations_seq'),'Iscaypata',145,now(),now()),
       (nextval('locations_seq'),'Independencia',123,now(),now());
-- Advances Project
insert into projects(project_id, responsible_id, state_id, location_id, project_code, assign_date, end_date, creation_ts, update_ts)
values (nextval('project_seq'),2,3,2,79878,now(),now(),now(),now());
-- Advances type
insert into advance_type(advance_type_id, description)
values (nextval('advance_type_id'),'Planificado'),
       (nextval('advance_type_id'),'En terreno'),
       (nextval('advance_type_id'),'Concluido'),
       (nextval('advance_type_id'),'Cancelado');
-- Advances
insert into advances(advance_id, type_id, project_id, start_time, end_time, creation_ts, update_ts)
values (nextval('advance_seq'),3,1,now(),now(),now(),now()),
       (nextval('advance_seq'),2,1,now(),now(),now(),now()),
       (nextval('advance_seq'),1,1,now(),now(),now(),now());
