-- Create EmployeePosition table
CREATE TABLE "employee_position" (
    "position_id" smallint PRIMARY KEY,
    "name" varchar(10)
);
-- create a sequence for EmployeePosition table
create sequence employee_position_seq as smallint increment 1;

--Create TypeProject table
CREATE TABLE advance_type (
   "advance_type_id" smallint PRIMARY KEY,
   "description" varchar(20) not null
);
-- create a sequence for TypeProject table
create sequence advance_type_id as smallint increment 1;

-- Create project_state table
CREATE TABLE "project_state" (
    "project_state_id" smallint PRIMARY KEY,
    "name" varchar(20)
);
-- create a sequence for project_state table
create sequence project_state_seq as smallint increment 1;

-- Create locations table
CREATE TABLE "locations" (
    "location_id" INTEGER PRIMARY KEY,
    "name" varchar(30),
    "distance" float,
    "creation_ts" timestamp,
    "update_ts" timestamp
);
-- create a sequence for locations table
create sequence locations_seq as integer increment 1;

-- Create vehicles table
CREATE TABLE "vehicles" (
    "vehicle_id" smallint PRIMARY KEY,
    "plate" varchar(8),
    "description" varchar(15),
    "color" varchar(10)
);
-- create a sequence for locations table
create sequence vehicles_seq as smallint increment 1;

-- Create Employees table
CREATE TABLE "employees" (
    "employee_id" integer PRIMARY KEY,
    "position_id" smallint,
    "names" varchar(30),
    "mother_last_name" varchar(15),
    "last_name" varchar(15),
    "id_card" varchar(10),
    "creation_ts" timestamp,
    "update_ts" timestamp
);
-- create a sequence for Employees table
create sequence employee_seq as integer increment 1;
-- Create Employees relations
ALTER TABLE "employees" ADD FOREIGN KEY ("position_id")
    REFERENCES "employee_position" ("position_id");

-- Create Quadrilles table
CREATE TABLE "quadrilles" (
    "quadrille_id" integer PRIMARY KEY,
    "vehicle_id" smallint,
    "code" varchar(5),
    "creation_ts" timestamp,
    "update_ts" timestamp
);
-- create a sequence for Quadrilles table
create sequence quadrille_seq as integer increment 1;
-- Create Quadrilles relations
ALTER TABLE "quadrilles" ADD FOREIGN KEY ("vehicle_id")
    REFERENCES "vehicles" ("vehicle_id");

--Create Quadrille members Table
CREATE TABLE "quadrille_members" (
    "employee_id" integer,
    "quadrille_id" integer,
    "update_ts" timestamp,
    PRIMARY KEY ("employee_id", "quadrille_id")
);
-- Create Quadrilles members relations
ALTER TABLE "quadrille_members" ADD FOREIGN KEY ("employee_id")
    REFERENCES "employees" ("employee_id");
ALTER TABLE "quadrille_members" ADD FOREIGN KEY ("quadrille_id")
    REFERENCES "quadrilles" ("quadrille_id");

-- Create Table Projects
CREATE TABLE "projects" (
  "project_id" integer PRIMARY KEY,
  "responsible_id" integer,
  "state_id" smallint,
  "location_id" integer,
  "project_code" integer not null ,
  "assign_date" date,
  "end_date" date,
  "creation_ts" timestamp,
  "update_ts" timestamp
);
-- create a sequence for project table
create sequence project_seq as integer increment 1;

-- Projects relations
ALTER TABLE "projects" ADD FOREIGN KEY ("responsible_id")
    REFERENCES "employees" ("employee_id");
ALTER TABLE "projects" ADD FOREIGN KEY ("state_id")
    REFERENCES "project_state" ("project_state_id");
ALTER TABLE "projects" ADD FOREIGN KEY ("location_id")
    REFERENCES "locations" ("location_id");

-- Create table Advances
CREATE TABLE "advances" (
    "advance_id" bigint PRIMARY KEY,
    "type_id" smallint,
    "project_id" integer,
    "start_time" timestamp not null ,
    "end_time" timestamp,
    "creation_ts" timestamp,
    "update_ts" timestamp
);
-- create a sequence for Advances table
create sequence advance_seq as bigint increment 1;
--Advances relations
ALTER TABLE "advances" ADD FOREIGN KEY ("project_id")
    REFERENCES "projects" ("project_id");
ALTER TABLE "advances" ADD FOREIGN KEY ("type_id")
    REFERENCES advance_type (advance_type_id) ;

-- Create table Advance Quadrille
CREATE TABLE "advance_quadrille" (
    "advance_id" bigint,
    "quadrille_id" integer,
    "update_ts" timestamp,
    PRIMARY KEY ("advance_id", "quadrille_id")
);
--advance_quadrille relations
ALTER TABLE "advance_quadrille" ADD FOREIGN KEY ("advance_id")
    REFERENCES "advances" ("advance_id");

ALTER TABLE "advance_quadrille" ADD FOREIGN KEY ("quadrille_id")
    REFERENCES "quadrilles" ("quadrille_id");

CREATE TABLE "advance_vehicle" (
    "vehicle_id" smallint,
    "advance_id" bigint,
    "update_ts" timestamp,
    "employees_qty" smallint,
    PRIMARY KEY ("vehicle_id", "advance_id")
);

ALTER TABLE "advance_vehicle" ADD FOREIGN KEY ("vehicle_id")
    REFERENCES "vehicles" ("vehicle_id");

ALTER TABLE "advance_vehicle" ADD FOREIGN KEY ("advance_id")
    REFERENCES "advances" ("advance_id");
